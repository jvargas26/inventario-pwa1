<?php

require './conexion.php';

$sql = "SELECT * FROM inventario";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $registros=array();
    $i=0;
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $registros[$i]=$row;
      $i++;
    }
    echo  json_encode($registros);
} else {
    echo "{}";
}

$conn->close();
?>