//PWA
navigator.serviceWorker.register('./service-worker.js');

function ToggleNav() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function Show_Hidden_Password() {
    var x = document.getElementById("myInput");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope, $http) {
    
    $scope.user="";
    $scope.password="";
    $scope.existeuser=function(){
        $http.get("https://angelsupport.000webhostapp.com/PWA/CHIDA/tesis-final/API/logp.php?user="+$scope.user+"&password="+$scope.password).then(function (response) {
        $scope.myData = response.data;
        console.log("Data= "+$scope.myData);
        console.log("Response= "+response.data);
            if(response.data !== null){
                sessionStorage.user = document.getElementById("user").value;
                sessionStorage.setItem("nombre", $scope.myData.nombre);
                        
                if($scope.myData.idtrabajador == $scope.user && $scope.myData.password == $scope.password){
                       
                 swal({
                        title: "Nos alegra tenerte aquí!!", 
                        text: "Bienvenido al inventario "+$scope.myData.nombre+".", 
                        icon: "success",
                        button: "Aceptar"
                    }).then( function() {
                        if($scope.myData.tipo == 0){
                            window.location.href="./newpassword.html";
                        }else if($scope.myData.tipo == 1){
                            window.location.href="./admin.html";
                        }else if($scope.myData.tipo == 2){
                            window.location.href="./custodio.html";
                        }
                   });
                }else{
                    swal({
                        title: "Algo salió mal!",
                        text: "El usuario y/o contraseña son incorrectos!!!",
                        icon: "error",
                        button: "Aceptar!",
                    }).then(function() {
                        // Redirect the user
                        window.location.href = "./index.html";
                    });
                }
            }else{
                swal({
                    title: "Algo salió mal!",
                    text: "Llena los campos del formulario para iniciar sesión!",
                    icon: "error",
                    button: "Aceptar.",
                }).then(function() {
                // Redirect the user
                    window.location.href = "./index.html";
                });
            }
        }); 
    };
    
    $scope.guardar=function(){
        //swal("Contraseña general cambiada", "Haz cambiado satisfactoriamente la contraseña general!", "success");
           
        $http.get("https://angelsupport.000webhostapp.com/PWA/tesis-final/API/cambiarcontrasena.php?contrasena="+$scope.contrasena).then(function (response) {
            $scope.myData = response.data;
           
        }); 
         swal("¡Contraseña general cambiada!", "Haz cambiado satisfactoriamente la contraseña general!", "success");
    };
    
    $scope.recupera=function(){
        $http.get("https://angelsupport.000webhostapp.com/PWA/tesis-final/API/cargar.php").then(function (response) {
            $scope.myData = response.data;
            $scope.contrasena =response.data;
        }); 
   };
   
    // setInterval(function(){$scope.recupera()}, 100);
    $scope.recupera();  

    $scope.valida=function(){
        if($scope.passwordn != $scope.passwordn2){
            swal({
                title: "¡Contraseña incorrecta!", 
                text: "Las no contraseñas coinciden: "+$scope.passwordn+".", 
                icon: "error",
                button: "Aceptar"
            });
        }else{
            $http.get("https://angelsupport.000webhostapp.com/PWA/tesis-final/API/newpassword.php?user="+sessionStorage.user+"&passwordn="+$scope.passwordn).then(function (response) {
                $scope.myData=response.data;
        });
            swal({
                title: "¡Contraseña correcta!", 
                text: "Las contraseñas coinciden: "+$scope.passwordn+".", 
                icon: "success",
                button: "Aceptar"
            }).then( function() {
                $scope.salir();
            });
        }
    };
    
    $scope.mobiliario=function(){
        $http.get("https://angelsupport.000webhostapp.com/PWA/tesis-final/API/mobiliario.php").then(function (response){
            $scope.myData = response.data;
            for(i=0; i >= $scope.myData.lenght; i++){
                console.log($scope.myData[i]);
            }
        });  
    };
    
    $scope.custodio=function(){
        $http.get("https://angelsupport.000webhostapp.com/PWA/tesis-final/API/custodio.php?user="+sessionStorage.getItem("user")).then(function (response) {
            $scope.myData = response.data;
        });   
    };
    
    $scope.usuario=function(){
        $http.get("https://angelsupport.000webhostapp.com/PWA/tesis-final/API/usuarios.php").then(function (response) {
            $scope.myData = response.data;
        });  
    };
    
    $scope.salir=function(){
            sessionStorage.user="";
            sessionStorage.contra="";
            sessionStorage.tipo="";
            sessionStorage.nombre="";
            
            window.location.href="index.html"; 
    };

});