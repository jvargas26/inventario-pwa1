let cacheName = 'InventarioPWA-v1';
let filesToCache = [
                        './',
                        './index.html',
                        './login.html',
                        './admin.html',
                        './js/app.js',
                        './js/jquery-strength.js',
                        './js/password_strength.js',
                        './css/login.css',
                        './css/admin.css',
                        './css/w3.css',
                        './css/nav.css',
                        './images/back-rugoso2.png',
                        './images/checkbox.png',
                        './images/logocircular.ico',
                        './images/logocircularwhite.png',
                        'https://fonts.googleapis.com/icon?family=Material+Icons',
                        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
                        'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
                        'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
                        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
                        'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js'
                        ];
        
self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(caches.match(event.request).then(function(response) {
    // caches.match() always resolves
    // but in case of success response will have value
    if (response !== undefined) {
      return response;
    } else {
      return fetch(event.request).then(function (response) {
        // response may be used only once
        // we need to save clone to put one copy in cache
        // and serve second one
        let responseClone = response.clone();
        
        caches.open('v1').then(function (cache) {
          cache.put(event.request, responseClone);
        });
        return response;
      }).catch(function () {
        return caches.match('/sw-test/gallery/myLittleVader.jpg');
      });
    }
  }));
});